# Scope Creep Tracker

A simple example that tags issues added to milestones after they start with the `scope-creep` label.

## Overview

1. A project webhook fires when a user updates an issue (e.g. adding the issue to a milestone), triggering the `scope-creep` ci pipeline
1. The `scope-creep` pipeline retrieves the issue metadata, the milestone metadata, and determines if the issue counts as scope creep.
1. If so, the pipeline adds the `scope-creep` label to the issue

## Setup

1. Create a [pipeline trigger](https://docs.gitlab.com/ee/ci/triggers/#use-a-webhook-payload), named: `scope-creep`
1. Create a [project webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) for the [issue event](https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#issue-events), and use the pipeline trigger url from above.
1. Create a milestone with a start date of yesterday
1. Create an issue and assign it to your milestone
1. ...
1. profit!

